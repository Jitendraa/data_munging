"""
To run the entire project simultaneouly, it is used.
"""
from soccer_league import soccer_main
from weather import weather_main

if __name__ == "__main__":
    weather_main()
    soccer_main()
