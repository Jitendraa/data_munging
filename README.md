# Data Munging
This project is used for the excercise at http://codekata.com/kata/kata04-data-munging/.

It mainly emphasizes the use of **Single Responsibility Principle** and **Open Close Principle** of SOLID concepts

## Environment
This project was developed in the follwing environment:
- Python 3.7.3 [GCC 7.3.0] :: Anaconda, Inc. on linux

## File structure
    data_munging
    ├── all_data_files
    |      ├── football.dat
    |      └── weather.dat
    ├── base.py
    ├── README.md
    ├── soccer_league.py
    └── weather.py

- football.dat -->  Contains football data of the  the results from the English Premier League for 2001/2. 
- weather.dat  -->  Contains the daily weather data for Morristown, NJ for June 2002.
- base.py (extractor) -->  Includes class for extraction from files and extracting particular columns from a matrix.
                           The classes for cleaning data, removing unwanted data and formatting data and computing minimum
                           difference spread between two columns of cleaned data(2-D list).
- soccer_league.py -->   Used for creating football data object and performing operation to compute minimum goals 
                        difference between for and agianst the team.
- weather.py --> Used for creating weather data object and gives you minimum temperature difference spread in the season.
- main.py --> It used run to entire project. It excutes all specified task simultaneouly.


## Usage
To run the project
>git clone https://gitlab.com/Jitendraa/data_munging.git

>cd data_munging/

>python main.py     (if you are using conda enviornment.)
