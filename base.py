"""
The modules deals with extraction of data from .dat file and can analyse the data set.

Classes:
    DataExtractor
    Analyser

functions:
    extract_data(filename) -> .dat file
    clean_data(extracted_data, first_index, second_index)
        -> 2-d array, index, index
    min_temp_spread_difference(cleaned_data, first_index, second_index)
        -> 2-d array, index, index
"""
from re import findall
import sys


class DataExtractor:
    """
    The class represents the extractor of data.
    ...
    Attributes:
    -----------
        filename : .dat file
    methods
    -------
    extract_data(filename):
        Extraction of data from .dat file.
    """

    def extract_data(self, filename):
        """
        Extraction of data from .dat file.

        parameters:
            filename (.dat): data set in the .dat file
        returns:
            lines (2-d list): contains row data
        """
        lines = [
            line.split() for line in open("./all_data_files/" + filename)
        ]
        return lines


class Analyser:
    """
    The analysis of the data after cleaing of the data.
    ...
    methods:
    --------
    clean_data(extracted_data, first_index, second_index)
        The required fields are filtered.

    min_diff_spread(cleaned_data, first_index, second_index)
        Finds out minimum difference spread and associated details.
    """
    def clean_data(self, extracted_data, first_index, second_index):
        """
        The required fields are cleaned.

        parameters:
            extracted_data(2-D list): An extracted row data from .data file
            first_index (index): index of the required element
            second_index (index): index of the required element

        returns:
            cleaned_data (2-D list) : cleaned data of required fields
        """
        cleaned_data = []
        for index in range(1, len(extracted_data)):
            if len(extracted_data[index]) < min(first_index, second_index):
                continue
            else:
                first_value = findall(
                    r"[-+]?\d*\.\d+|\d+", extracted_data[index][first_index]
                )[0]
                second_value = findall(
                    r"[-+]?\d*\.\d+|\d+", extracted_data[index][second_index]
                )[0]

                if(first_value.isdigit() or
                   second_value.isdigit() or
                   first_value.replace('.', "", 1).isdigit() or
                   second_value.replace('.', "", 1).isdigit()):

                    extracted_data[index][first_index] = first_value
                    extracted_data[index][second_index] = second_value
                    cleaned_data.append(extracted_data[index])

        return cleaned_data

    def min_diff_spread(self, cleaned_data, first_index, second_index):
        """
        The function gives minimum difference spread between two columns.

        parameters:
            cleaned_data (2-D list) : cleaned data of required fields
            first_index (index): index of the required element
            second_index (index): index of the required element

        returns:
        (item1, item2, int(min_absolute_diff), result_row) tuple : deatails
        """
        min_absolute_diff = sys.maxsize
        result_row = []
        item1 = ''
        item2 = ''

        for data_row in cleaned_data:
            abs_diff = abs(
                float(data_row[first_index]) - float(data_row[second_index])
            )
            if abs_diff < min_absolute_diff:
                min_absolute_diff = abs_diff
                item1 = data_row[first_index]
                item2 = data_row[second_index]
                result_row = data_row

        return (item1, item2, int(min_absolute_diff), result_row)
