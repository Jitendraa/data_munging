"""
The modules deals with min temperature spread June 2002, for Morristown.

classes:
     WeatherAnalyser

 functions:
        min_temp_spread_details(file_name) -> .dat file
        weather_main()
"""
from base import DataExtractor, Analyser


class WeatherAnalyser(DataExtractor, Analyser):
    """
    The class deals with minimum goals difference between two columns.

    Classes:
    SoccerAnalyser
    ...
    methods:
    --------
        min_temp_spread_details(file_name) -> .dat file
        main()
    """
    def min_temp_spread_details(self, file_name):
        """
        Retuns minimum temperature difference betwen two attributes.

        parameters:
            file_name (.dat file): data for Morristown, NJ for June 2002.
        """
        raw_data = super().extract_data(file_name)
        cleaned_data = super().clean_data(raw_data, 1, 2)
        detail_result = super().min_diff_spread(cleaned_data, 1, 2)

        return detail_result


def weather_main():
    """
    This shows the details of minimum goals spread.

    returns:
        None
    """
    weather_analysis = WeatherAnalyser()
    detail_data = weather_analysis.min_temp_spread_details('weather.dat')

    print(
        "  Day Number:", detail_data[-1][0],
        "  Max_temperatre:", detail_data[0],
        "  Min_temperature:", detail_data[1],
        "  Temp-difference:", detail_data[2]
    )
