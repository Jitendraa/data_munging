"""
The module deals with minimum goal differnece spread in  2001 for
English Premier League.

Classes:
    SoccerAnalyser

functions:
    smallest_diff_in_goals(file_name) -> .dat file
    soccer_main()
"""
from base import DataExtractor, Analyser


class SoccerAnalyser(DataExtractor, Analyser):
    """
    The class gives an analysis of min goals spread between two columns.
    ...
    methods:
    --------
        smallest_diff_in_goals(file_name)
    """
    def smallest_diff_in_goals(self, file_name):
        """
        Computes samllest difference in goals.

        parameter:
            file_name (.data file): contains the results from the
                                    English Premier League for 2001.
        returns:
            detail_result (tuple): minimum difference with associated details.
        """
        raw_data = super().extract_data(file_name)
        cleaned_data = super().clean_data(raw_data, 6, 8)
        detail_result = super().min_diff_spread(cleaned_data, 6, 8)

        return detail_result


def soccer_main():
    """
    This shows the details of minimum goals spread.

    returns:
        None
    """
    soccer_analysis = SoccerAnalyser()
    detail_data = soccer_analysis.smallest_diff_in_goals('football.dat')

    print(
        "  Team Name:", detail_data[-1][1],
        "  For Goals:", detail_data[0],
        "  Against Goals:", detail_data[1],
        "  Goal-difference:", detail_data[2]
    )
